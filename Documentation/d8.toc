\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {subsection}{\numberline {1.1}Identification}{2}
\contentsline {subsection}{\numberline {1.2}Related Documentation}{2}
\contentsline {subsection}{\numberline {1.3}Purpose and Description of Document}{2}
\contentsline {subsection}{\numberline {1.4}Document Status and Schedule}{2}
\contentsline {section}{\numberline {2}Component Diagram}{4}
\contentsline {section}{\numberline {3}State Chart Diagrams}{5}
\contentsline {subsection}{\numberline {3.1}Utility}{5}
\contentsline {subsection}{\numberline {3.2}Advertisement}{6}
\contentsline {subsection}{\numberline {3.3}Offer}{7}
\contentsline {section}{\numberline {4}Class Diagrams}{8}
\contentsline {subsection}{\numberline {4.1}Utility}{8}
\contentsline {subsection}{\numberline {4.2}Advertisement}{9}
\contentsline {subsection}{\numberline {4.3}Offer}{10}
\contentsline {subsection}{\numberline {4.4}Visit}{11}
\contentsline {section}{\numberline {5}System API}{12}
\contentsline {subsection}{\numberline {5.1}UtilityAPI}{12}
\contentsline {subsection}{\numberline {5.2}AdvertisementAPI}{14}
\contentsline {subsection}{\numberline {5.3}OfferAPI}{18}
\contentsline {subsection}{\numberline {5.4}VisitAPI}{20}
\contentsline {section}{\numberline {6}Testing Policy}{21}
\contentsline {section}{\numberline {7}Integration Tests}{21}
\contentsline {section}{\numberline {8}Regression Testing}{21}
\contentsline {section}{\numberline {9}Acceptance Test Cases}{21}
\contentsline {subsection}{\numberline {9.1}Utility Test Cases}{21}
\contentsline {subsection}{\numberline {9.2}Advertisement Management Test Cases}{24}
\contentsline {subsection}{\numberline {9.3}Offer Management Test Cases}{26}
\contentsline {subsection}{\numberline {9.4}Visit Management Test Cases}{27}
\contentsline {section}{\numberline {10}Acceptance Test Report}{28}
\contentsline {subsection}{\numberline {10.1}Test case execution and correctness}{28}
\contentsline {subsection}{\numberline {10.2}Test case implementation}{29}
\contentsline {section}{\numberline {11}Maintenance Report}{35}
