\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {subsection}{\numberline {1.1}Identification}{2}
\contentsline {subsection}{\numberline {1.2}Related Documentation}{2}
\contentsline {subsection}{\numberline {1.3}Purpose and Description of Document}{2}
\contentsline {subsection}{\numberline {1.4}Document Status and Schedule}{2}
\contentsline {section}{\numberline {2}Component Diagram}{3}
\contentsline {section}{\numberline {3}State Chart Diagrams}{4}
\contentsline {subsection}{\numberline {3.1}Utility}{4}
\contentsline {subsection}{\numberline {3.2}Advertisement}{5}
\contentsline {subsection}{\numberline {3.3}Offer}{6}
\contentsline {section}{\numberline {4}Class Diagrams}{7}
\contentsline {subsection}{\numberline {4.1}Utility}{7}
\contentsline {subsection}{\numberline {4.2}Advertisement}{8}
\contentsline {subsection}{\numberline {4.3}Offer}{9}
\contentsline {subsection}{\numberline {4.4}Visit}{10}
\contentsline {section}{\numberline {5}System API}{11}
\contentsline {subsection}{\numberline {5.1}UtilityAPI}{11}
\contentsline {subsection}{\numberline {5.2}AdvertisementAPI}{13}
\contentsline {subsection}{\numberline {5.3}OfferAPI}{17}
\contentsline {subsection}{\numberline {5.4}VisitAPI}{19}
\contentsline {section}{\numberline {6}Testing Policy}{20}
\contentsline {section}{\numberline {7}Integration Tests}{20}
\contentsline {section}{\numberline {8}Regression Testing}{20}
\contentsline {section}{\numberline {9}Acceptance Test Cases}{20}
\contentsline {subsection}{\numberline {9.1}Utility Test Cases}{20}
\contentsline {subsection}{\numberline {9.2}Advertisement Management Test Cases}{21}
\contentsline {subsection}{\numberline {9.3}Offer Management Test Cases}{23}
\contentsline {subsection}{\numberline {9.4}Visit Management Test Cases}{23}
