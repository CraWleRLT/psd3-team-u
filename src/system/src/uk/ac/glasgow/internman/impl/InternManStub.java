package uk.ac.glasgow.internman.impl;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import uk.ac.glasgow.AdvertisementManagement.Advertisement;
import uk.ac.glasgow.AdvertisementManagement.AdvertisementStoreImpl;
import uk.ac.glasgow.AdvertisementManagement.Employer;
import uk.ac.glasgow.internman.InternMan;
import uk.ac.glasgow.AdvertisementManagement.Role;
import uk.ac.glasgow.OfferManagement.Offer;
import uk.ac.glasgow.OfferManagement.OfferStoreImpl;
import uk.ac.glasgow.internman.Student;
import uk.ac.glasgow.internman.UoGGrade;
import uk.ac.glasgow.Utility.UtilityAPI;
import uk.ac.glasgow.Utility.Utility;
import uk.ac.glasgow.VisitManagement.VisitAPI;
import uk.ac.glasgow.VisitManagement.VisitAPIImpl;
import uk.ac.glasgow.Utility.User;
import uk.ac.glasgow.AdvertisementManagement.Internship;
import uk.ac.glasgow.Utility.StudentImpl;
import uk.ac.glasgow.Utility.User;

/**
 * A stub class that implements the InternMan facade without providing any
 * actual functionality (except permitting logging in and out). The class should
 * be replaced by a real implementation once development begins.
 * 
 * @author Cowboy
 * 
 */
public class InternManStub implements InternMan {
	private UtilityAPI utilityComponent = new Utility();
	private VisitAPI visitComponent = new VisitAPIImpl();
	
	public InternManStub(){}   
	
	@Override
	public boolean login(String userName, String password) {
		return utilityComponent.login(userName, password);
	}

	@Override
	public User getCurrentUser() {
		return utilityComponent.getCurrentUser();
	}

	@Override
	public Map<Integer, Advertisement> getAdvertisements() {
		Map<Integer, Advertisement> tmp = null;
		AdvertisementStoreImpl adStore = new AdvertisementStoreImpl("data/adverts.obj");
			if (utilityComponent.isLogged()) {
                if(utilityComponent.getCurrentUser().getLevel().equals("coursecoordinator") || utilityComponent.getCurrentUser().getLevel().equals("student")){
                    return adStore.getAdverts();
                }
                else if(utilityComponent.getCurrentUser().getLevel().equals("Employer")){
                    HashMap<Integer, Advertisement> store = new HashMap<Integer, Advertisement>();
                    tmp = adStore.getAdverts();
                    int i = 0;
                    for(Advertisement a : tmp.values()){
                        if(a.getEmployer() == getCurrentEmployer()){
                            tmp.put(i, a);
                        }
                        i++;
                    }
                    return tmp;
                }
			}
       return tmp;
	}

	@Override
	public Map<String, Student> getStudents() {
		return utilityComponent.displayStudentSummary();
	}

	@Override
	public Employer registerNewEmployer(String name, String emailAddress) {
            if(utilityComponent.getCurrentUser().getLevel().equals("coursecoordinator")){
                Employer e = new Employer(name, emailAddress, emailAddress, name);
                utilityComponent.getStore().addUser(new User(name, null, name, emailAddress, "Employer"));
                return e;
            }
            else{
                return null;
            }
	}

	@Override
	public Advertisement createNewAdvertisement(String applicationDetails) {
            if(utilityComponent.getCurrentUser().getLevel().equals("Employer")){
		Advertisement adv = new Advertisement(applicationDetails);          
                return adv;
            }
            else{
                return null;
            }
	}

	@Override
	public Advertisement selectAdvertisement(Integer index) {
            if(utilityComponent.getCurrentUser().getLevel().equals("Employer") || utilityComponent.getCurrentUser().getLevel().equals("student") || utilityComponent.getCurrentUser().getLevel().equals("coursecoordinator")){
		AdvertisementStoreImpl adStore = new AdvertisementStoreImpl("data/adverts.obj");
                return adStore.getAdvert(index);
            }
            else{
                return null;
            }
	}

	@Override
	public Role selectRole(Integer advertisementIndex, Integer roleIndex) {
		AdvertisementStoreImpl adStore = new AdvertisementStoreImpl("data/adverts.obj");
                return adStore.getAdvert(advertisementIndex).getRoles().get(roleIndex);
	}

	@Override
	public Student selectStudent(String matriculation) {
		Map<String, Student> students = utilityComponent.displayStudentSummary();
		if (students == null) {
			return null;
		} 
		return students.get(matriculation);
	}

	@Override
	public void publishAdvertisement(Integer advertisementIndex, String comment) {
		Advertisement ad = selectAdvertisement(advertisementIndex);
                ad.setStatus(Advertisement.AdvertisementStatus.PUBLISHED);
                ad.setComments(comment);
		
	}

	@Override
	public void notifyAcceptedOffer(Role role, String managerName,
			String managerEmail) {
		System.out.println("Message sent to " + managerName + " at " + managerEmail);
		
	}

	@Override
	public void approveAcceptedOffer(String matriculation) {
	    OfferStoreImpl offers = new OfferStoreImpl("data/offers.obj");
            Offer offer = offers.getOffer(matriculation);
            offer.getInternship().setStatus(Internship.InternshipStatus.APPROVED);
		
	}

	@Override
	public void assignAcademicVisitor(String matriculation, String visitorName) {
		Map<String, Student> students = utilityComponent.displayStudentSummary();
		if (students == null) {
			return;
		}
		Student student = students.get(matriculation);
		if (student != null) {
			visitComponent.assignAcademicVisitor(student, visitorName, "");
		}
	}

	@Override
	public void recordVisitAssessment(String matriculation, UoGGrade grade,
			String description, int internshipIndex) {
		Map<String, Student> students = utilityComponent.displayStudentSummary();
		if (students == null) {
			return;
		}
		Student student = students.get(matriculation);
		if (student != null) {
			visitComponent.recordVisitAssessment(student, grade, description, internshipIndex);
		}
	}

	@Override
	public Employer getCurrentEmployer() {
            if(utilityComponent.isLogged()) {
            	if(utilityComponent.getCurrentUser().getLevel().equals("Employer")){
            		return new Employer(utilityComponent.getCurrentUser().getMatriculation(), utilityComponent.getCurrentUser().getPassword(), utilityComponent.getCurrentUser().getPassword(), utilityComponent.getCurrentUser().getMatriculation());
            	}
            }
            return null;
        }

	@Override
	public Role createNewSelfSourcedRole(String title, String location,
			Date start, Date end, String description, Double salary) {
		Role r = new Role(title, location, start, end , description, salary);
                return r;
	}
}
