package uk.ac.glasgow.internman;

import java.util.ArrayList;

import uk.ac.glasgow.internman.Internship;

public interface Student {

	ArrayList<Internship> getInternships();

	String getSurname();

	String getMatriculation();

	String getForename();

	String getEmail();

	String getProgramme();
	
	void addInternship(Internship internship);
	
	boolean sufficientInternships();

}
