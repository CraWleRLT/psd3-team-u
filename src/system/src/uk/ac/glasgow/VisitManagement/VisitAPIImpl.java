package uk.ac.glasgow.VisitManagement;

import java.util.ArrayList;

import uk.ac.glasgow.Utility.Utility;
import uk.ac.glasgow.Utility.UserStoreImpl;
import uk.ac.glasgow.Utility.VisitorImpl;
import uk.ac.glasgow.internman.UoGGrade;
import uk.ac.glasgow.internman.Visitor;
import uk.ac.glasgow.internman.Visit;
import uk.ac.glasgow.internman.Student;
import uk.ac.glasgow.internman.Internship;
import uk.ac.glasgow.internman.InternshipImpl;

public class VisitAPIImpl implements VisitAPI {
	
	public VisitAPIImpl() {}
	
	public void assignAcademicVisitor ( Student student, String visitorName, String visitorEmail ) {
		ArrayList<Internship> internships = student.getInternships();
		if (internships != null) {
			VisitorImpl visitor = new VisitorImpl( visitorName, visitorEmail);
			for (Internship intern : internships) {
				if (intern != null) {
					if(intern.getVisit() == null) {
						Visit visit = new VisitImpl(visitor);
						if(!Utility.users.getUsers().containsKey(visitor.getMatriculation())) {
							Utility.users.addUser(visitor);
						}
						intern.setVisit(visit);
					}
				}
			}
		}
	}
	
	public void recordVisitAssessment ( Student student, UoGGrade grade, String description, int internshipIndex ) {
		if (student.getInternships().size() > internshipIndex) {
			Internship internship = student.getInternships().get(internshipIndex);
			Visit visit = internship.getVisit();
			if (visit != null) {
				visit.recordVisitAssessment(grade, description);
			}
		} else {
			System.out.println("Index out of bounds. Internship doesn't exist.");
		}
	}
	
}
