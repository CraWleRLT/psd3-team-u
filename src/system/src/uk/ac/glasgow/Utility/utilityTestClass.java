package uk.ac.glasgow.Utility;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


/*a class to test the functionality of the Utility component in the system*/
public class utilityTestClass {
	
	private static Utility u;
	private static UserStore users;
	
	@BeforeClass
	 public static void testSetup() {
		//instantiate new utility instance to test upon
		 u = new Utility();
	 }
	
	@Test
	public void testStudentLogin(){
		boolean result;
		users = u.getUsers();
		users.addUser("Snares", "Venetian","1045957d" , "gnm38kn", "student");
		result = u.login("1045957d", "gnm38kn");
		assertEquals("",true,result);
	}
	
	@Test
	public void testCompanyLogin(){
		boolean result;
		users = u.getUsers();
		users.addUser("Smith", "IBM","089231x" , "12345", "company");
		result = u.login("089231x", "12345");
		assertEquals("",true,result);
	}
	
	
	@Test
	public void testAdminLogin(){
		boolean result;
		users = u.getUsers();
		users.addUser("", "Admin","0712312y" , "password", "admin");
		result = u.login("0712312y", "password");
		assertEquals("",true,result);
	}
	
	
	
	
	//use cases added since original acceptance-test cases


	@Test
	//tests a specific type of user saved is returned correctly
	public void testTypeLogin(){
		String result;
		users = u.getUsers();
		users.addUser("", "","username" , "password", "test-type");
		u.login("username", "password");
		result = u.userLevel();
		assertEquals("","test-type",result);
	}
	
	

}