package uk.ac.glasgow.Utility;

import java.io.Serializable;
import java.util.ArrayList;

import uk.ac.glasgow.Utility.User;
import uk.ac.glasgow.internman.Student;
import uk.ac.glasgow.internman.Internship;
import uk.ac.glasgow.internman.InternshipImpl;


public class StudentImpl extends User implements Student, Serializable {
	static final long serialVersionUID = 2L;
   private String status;
   private String email;
   private String program;
   private ArrayList<Internship> internships;
   
   public StudentImpl(String surname, String forename, String GUID, String password, String user_level, String status, String email, String program) {
	  super(surname, forename, GUID, password, user_level);
      this.status = status;
      this.email = email;
      this.program = program;
      this.internships = new ArrayList<Internship>();
   }

   public String getStatus() {
      return status;
   }

	public String getEmail() {
		return email;
	}

	public String getProgramme() {
		return program;
	}
	
	public void addInternship(Internship internship) {
		this.internships.add(internship);  
	}
	
	public ArrayList<Internship> getInternships() {
		return internships;
	}
	
	public boolean sufficientInternships() {
		long weeks = 0;
		if(internships != null) {
			for (Internship intern : internships) {
				weeks = weeks + (intern.getRole().getEnd().getTime() - intern.getRole().getStart().getTime())/(7 * 24 * 60 * 60 * 1000);
			}
			return (weeks > 12);
		}
		return false;
	}

}