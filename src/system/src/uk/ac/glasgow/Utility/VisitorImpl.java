package uk.ac.glasgow.Utility;

import uk.ac.glasgow.internman.Visitor;
import java.io.Serializable;
import java.lang.Math;
import uk.ac.glasgow.Utility.UserStore;
import uk.ac.glasgow.Utility.UserStoreImpl;

public class VisitorImpl extends User implements Visitor, Serializable {
	static final long serialVersionUID = 3L;
	private String visitorName;
	private String visitorEmail;
	
	public VisitorImpl(String visitorName, String visitorEmail) {
		// visitorName.replaceAll("[^\\p{L]\\{N}]", "") - remove all characters that are neither letters nor numbers
		// (Math.round(Math.random() * 89999999) + 10000000) + "" - generates a random 8 digit number, which is used as a password
		super(visitorName, "", visitorName.replaceAll("[^\\p{L]\\{N}]", "").toLowerCase(), (Math.round(Math.random() * 89999999) + 10000000) + "", "visitor");
		this.visitorName = visitorName;
		this.visitorEmail = visitorEmail;
	}
	
	public String getName() {
		return visitorName;
	}

}
